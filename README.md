# eth-monitor

To deploy on your google account, just build on your machine and follow the regular firebase app configs for firebase functions.

## to query

Fire a POST against the firebase function address you've got, adding to the body:

    {
	    "address": "0xaa08e0d0ec182e1ecd54bca58883739904e954ed", // contract address
	    "fields": ["getProperty", "owner"], // list of view functions or public properties to read
	    "infura_token": "OpYNeIJUREK8mpgQtiId", // your infura token
	    "network": "ropsten", // network
	    "abi": [{...}] // the contract abi
    }